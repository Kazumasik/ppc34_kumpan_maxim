package ex01;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import junit.framework.Assert;
import java.io.IOException;
import ex01.Calc;

/**
 * Выполняет тестирование разработанных классов.
 * 
 * @author Maxim4ik
 * @version 1.0
 */
public class MainTest {
	/** Проверка основной функциональности класса {@linkplain Calc} */
	@Test
	public void testCalc() {
		Calc calc = new Calc();
		calc.init(Integer.toBinaryString(3));
		assertEquals(3.897, calc.getResult().getArea(), .1e-2);
		calc.init(Integer.toBinaryString(6));
		assertEquals(15.588, calc.getResult().getArea(), .1e-2);
		calc.init(Integer.toBinaryString(9));
		assertEquals(35.074, calc.getResult().getArea(), .1e-2);
		calc.init(Integer.toBinaryString(12));
		assertEquals(62.353, calc.getResult().getArea(), .1e-2);
		calc.init(Integer.toBinaryString(15));
		assertEquals(97.427, calc.getResult().getArea(), .1e-2);
	}

	/** Проверка сериализации. Корректность восстановления данных. */
	@Test
	public void testRestore() {
		Calc calc = new Calc();
		String ranLength;
		double sumArea;
		for (int ctr = 0; ctr < 1000; ctr++) {
			ranLength = Integer.toBinaryString((int)(Math.random() * 20)+1);
			sumArea = calc.init(ranLength);
			try {
				calc.save();
			} catch (IOException e) {
				Assert.fail(e.getMessage());
			}
			calc.init(Integer.toBinaryString((int)(Math.random() * 20)+1));
			try {
				calc.restore();
			} catch (Exception e) {
				Assert.fail(e.getMessage());
			}
			assertEquals(sumArea, calc.getResult().getArea(),.1e-10);

			assertEquals(ranLength, calc.getResult().getLength());
		}
	}
}