package ex02;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import junit.framework.Assert;
import java.io.IOException;
import ex01.Item2d;
/** Выполняет тестирование
 * разработанных классов.
 * @author Maxim4ik
 * @version 2.0
 */
public class MainTest {
    /** Проверка основной функциональности класса {@linkplain ViewResult} */
    @Test
    public void testCalc() {
        ViewResult view = new ViewResult(5);
        view.init(Integer.toBinaryString(5));
        Item2d item = new Item2d();
        int ctr = 0;
        item.setLengthArea("0", 0.0);
        assertTrue("expected:<" + item + "> but was:<" + view.getItems().get(ctr) + ">",
                view.getItems().get(ctr).equals(item));
        ctr++;
        item.setLengthArea("1100101", 4417.162572002529);
        assertTrue("expected:<" + item + "> but was:<" + view.getItems().get(ctr) + ">",
                view.getItems().get(ctr).equals(item));
        ctr++;
        item.setLengthArea("11001010", 17668.650288010114);
        assertTrue("expected:<" + item + "> but was:<" + view.getItems().get(ctr) + ">",
                view.getItems().get(ctr).equals(item));
        ctr++;
        item.setLengthArea("100101111", 39754.46314802276);
        assertTrue("expected:<" + item + "> but was:<" + view.getItems().get(ctr) + ">",
                view.getItems().get(ctr).equals(item));
        ctr++;
        item.setLengthArea("110010100", 70674.60115204046);
        assertTrue("expected:<" + item + "> but was:<" + view.getItems().get(ctr) + ">",
                view.getItems().get(ctr).equals(item));
    }
    /** Проверка сериализации. Корректность восстановления данных. */
    @Test
    public void testRestore() {
        ViewResult view1 = new ViewResult(1000);
        ViewResult view2 = new ViewResult();
// Вычислим значение функции со случайным шагом приращения аргумента
        view1.init(Integer.toBinaryString((int)(Math.random() * 20)+1));
// Сохраним коллекцию view1.items
        try {
            view1.viewSave();
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
// Загрузим коллекцию view2.items
        try {
            view2.viewRestore();
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
// Должны загрузить столько же элементов, сколько сохранили
        assertEquals(view1.getItems().size(), view2.getItems().size());
// Причем эти элементы должны быть равны.
// Для этого нужно определить метод equals
        assertTrue("containsAll()", view1.getItems().containsAll(view2.getItems()));
    }
}