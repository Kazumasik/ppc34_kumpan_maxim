package ex01;

import java.io.Serializable;

/**
 * Хранит исходные данные и результат вычислений.
 * 
 * @author Maxim4ik
 * @version 1.0
 */
public class Item2d implements Serializable {
	/** Аргумент вычисляемой функции. */
// transient
	private String ranLength;
	/** Результат вычисления функции. */
	private double sumArea;

	/** Автоматически сгенерированная константа */
	private static final long serialVersionUID = 1L;

	/** Инициализирует поля {@linkplain Item2d#ranLength}, {@linkplain Item2d#sumArea} */
	public Item2d() {
		ranLength = "";
		sumArea = 0;
	}

	/**
	 * Устанавливает значения полей: аргумента и результата вычисления функции.
	 * 
	 * @param ranLength - значение для инициализации поля {@linkplain Item2d#ranLength}
	 * @param sumArea - значение для инициализации поля {@linkplain Item2d#sumArea}
	 */
	public Item2d(String ranLength, double sumArea) {
		this.ranLength = ranLength;
		this.sumArea = sumArea;
	}

	/**
	 * Установка значения поля {@linkplain Item2d#ranLength}
	 * 
	 * @param ranLength - значение для {@linkplain Item2d#ranLength}
	 * @return Значение {@linkplain Item2d#ranLength}
	 */
	public String setLength(String ranLength) {
		return this.ranLength = ranLength;
	}

	/**
	 * Получение значения поля {@linkplain Item2d#ranLength}
	 * 
	 * @return Значение {@linkplain Item2d#ranLength}
	 */
	public String getLength() {
		return ranLength;
	}

	/**
	 * Установка значения поля {@linkplain Item2d#sumArea}
	 * 
	 * @param sumArea - значение для {@linkplain Item2d#sumArea}
	 * @return Значение {@linkplain Item2d#sumArea}
	 */
	public double setArea(double sumArea) {
		return this.sumArea = sumArea;
	}

	/**
	 * Получение значения поля {@linkplain Item2d#sumArea}
	 * 
	 * @return значение {@linkplain Item2d#sumArea}
	 */
	public double getArea() {
		return sumArea;
	}

	/**
	 * Установка значений {@linkplain Item2d#ranLength} и {@linkplain Item2d#sumArea}
	 * 
	 * @param ranLength - значение для {@linkplain Item2d#ranLength}
	 * @param sumArea - значение для {@linkplain Item2d#sumArea}
	 * @return this
	 */
	public Item2d setLengthArea(String ranLength, double sumArea) {
		this.ranLength = ranLength;
		this.sumArea = sumArea;
		return this;
	}

	/**
	 * Представляет результат вычислений в виде строки.<br>
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "Length = " + ranLength + ", sum of areas = " + sumArea;
	}

	/**
	 * Автоматически сгенерированный метод.<br>
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		Item2d other = (Item2d) obj;
		if (ranLength!= other.ranLength)
			return false;
// изменено сравнение результата вычисления функции
		if (sumArea!= other.sumArea)
			return false;
		return true;
	}
}