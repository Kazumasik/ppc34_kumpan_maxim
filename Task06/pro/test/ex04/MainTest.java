package ex04;
import static org.junit.Assert.*;
import org.junit.Test;
import ex01.Item2d;
import ex02.ViewResult;
/** Тестирование класса
 * ChangeItemCommand
 * @author xone
 * @version 4.0
 * @see ChangeItemCommand
 */
public class MainTest {
    /** Проверка метода {@linkplain ChangeItemCommand#execute()} */
    @Test
    public void testExecute() {
        ChangeItemCommand cmd = new ChangeItemCommand();
        cmd.setItem(new Item2d());
        double y, offset;
        String x;
        for (int ctr = 0; ctr < 1000; ctr++) {
            cmd.getItem().setLengthArea(x=Integer.toBinaryString((int)( Math.random() * 100.0)), y = Math.random() * 100.0);
            cmd.setOffset(offset = Math.random() * 100.0);
            cmd.execute();
            assertEquals(x, cmd.getItem().getLength());
            assertEquals(y * offset, cmd.getItem().getArea(), .1e-10);
        }
    }

    /** Проверка класса {@linkplain ChangeConsoleCommand} */
    @Test
    public void testChangeConsoleCommand() {
        ChangeConsoleCommand cmd = new ChangeConsoleCommand(new ViewResult());
        cmd.getView().viewInit();
        cmd.execute();
        assertEquals("'c'hange", cmd.toString());
        assertEquals('c', cmd.getKey());
    }
}
